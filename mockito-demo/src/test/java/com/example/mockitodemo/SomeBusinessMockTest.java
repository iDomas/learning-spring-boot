package com.example.mockitodemo;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class SomeBusinessMockTest {

	@Test
	public void testFindTheGreatestFromAllData() {
		DataService dataServiceMock = mock(DataService.class);
		when(dataServiceMock.retrieveAllData()).thenReturn(new int[] {3, 4, 5});

		SomeBusinessImpl businessImpl = new SomeBusinessImpl(dataServiceMock);

		int result = businessImpl.findTheGreatestFromAllData();
		assertEquals(5, result);
	}

	@Test
	public void testFindTheGreatestFromAllData_OneValue() {
		DataService dataServiceMock = mock(DataService.class);
		when(dataServiceMock.retrieveAllData()).thenReturn(new int[] {5});

		SomeBusinessImpl businessImpl = new SomeBusinessImpl(dataServiceMock);

		int result = businessImpl.findTheGreatestFromAllData();
		assertEquals(5, result);
	}

	@Test
	public void testFindTheGreatestFromAllData_NoValue() {
		DataService dataServiceMock = mock(DataService.class);
		when(dataServiceMock.retrieveAllData()).thenReturn(new int[] {});

		SomeBusinessImpl businessImpl = new SomeBusinessImpl(dataServiceMock);

		int result = businessImpl.findTheGreatestFromAllData();
		assertEquals(5, result);
	}
}
