package com.example.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
		ArraysCompareTest.class,
		QuickBeforeAfterTest.class,
		StringHelperPatameterizedTest.class,
		StringHelperTest.class})
public class AllTests {

}
