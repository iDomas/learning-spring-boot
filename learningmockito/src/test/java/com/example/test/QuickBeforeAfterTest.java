package com.example.test;

import org.junit.*;

public class QuickBeforeAfterTest {

	@BeforeClass
	public static void beforeClass() {
		System.out.println("Before class");
	}

	@Before
	public void setup() {
		System.out.println("Before TEST");
	}

	@Test
	public void test1() {
		System.out.println("test1 executed");
	}

	@Test
	public void test2() {
		System.out.println("test2 executed");
	}

	@After
	public void clean() {
		System.out.println("After test");
	}

	@AfterClass
	public static void afterClass() {
		System.out.println("After class");
	}
}
