package com.example.business;

import com.example.data.api.TodoService;
import com.example.data.api.TodoServiceStub;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class TodoBusinessImplStubTest {
	private TodoService todoServiceStub = new TodoServiceStub();

	@Test
	public void testRetrieveTodosRelatedToSpring_usingAStub() {
		TodoBusinessImpl todoBusiness =
				new TodoBusinessImpl(todoServiceStub);

		List<String> filteredTodos = todoBusiness.retrieveTodosRelatedToSpring("Dummy");

		assertEquals(2, filteredTodos.size());
	}

	@Test
	public void testRetrieveTodosRelatedToSpring_isContentEqualToGiven() {
		List<String> expected = Arrays.asList("Learn Spring MVC", "Learn Spring");

		TodoBusinessImpl todoBusiness =
				new TodoBusinessImpl(todoServiceStub);

		List<String> actual = todoBusiness.retrieveTodosRelatedToSpring("Dummy");
		assertEquals(expected, actual);
	}
}
