package com.example.controller;

import com.example.model.Car;
import com.example.service.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CarController {

	@Autowired
	private CarService carService;

	@GetMapping("/cars")
	public List<Car> retrieveCars() {
		return carService.retrieveCars();
	}

	@GetMapping("/cars/{carId}")
	public Car retrieveCar(@PathVariable String carId) {
		return carService.retrieveCar(carId);
	}

	@PostMapping("/cars")
	public Car addCar(@RequestBody Car car) {
		carService.addCar(car);
		return car;
	}

}
