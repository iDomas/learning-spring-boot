package com.example.jpa;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class UserCommandLineRunner implements CommandLineRunner {

	private static final Logger LOG = LoggerFactory.getLogger(UserCommandLineRunner.class);

	@Autowired
	private UserRepository userRepository;

	@Override
	public void run(String... strings) throws Exception {
		userRepository.save(new User("Ranga", "Admin"));
		userRepository.save(new User("Ravi", "User"));
		userRepository.save(new User("Domas", "Admin"));
		userRepository.save(new User("Raghu", "User"));

		for(User user : userRepository.findAll())
			LOG.info("{}", user);

		LOG.info("----------------------------");
		LOG.info("			FIND ONE");
		LOG.info("{}", userRepository.findOne(3L));
		LOG.info("----------------------------");
		LOG.info("			EXISTS");
		LOG.info("{}", userRepository.exists(5L));
		LOG.info("----------------------------");
		LOG.info("			COUNT");
		LOG.info("{}", userRepository.count());
		LOG.info("----------------------------");
		LOG.info("			DELETE");
		userRepository.delete(3L);
		LOG.info("----------------------------");
		LOG.info("			SAVE");
		userRepository.save(new User("Gambino", "User"));
		LOG.info("----------------------------");
		LOG.info("			ALL");
		for(User user : userRepository.findAll())
			LOG.info("{}", user);
		LOG.info("----------------------------");
		LOG.info("			Admins are");
		for(User user : userRepository.findByRole("Admin"))
			LOG.info("{}", user);
		LOG.info("----------------------------");

	}
}
