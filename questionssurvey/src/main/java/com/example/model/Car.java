package com.example.model;

import java.util.Objects;

public class Car {

	private String id;
	private String brand;
	private String model;
	private Integer year;
	private String description;

	public Car() {}

	public Car(String id, String brand, String model, Integer year, String description) {
		this.id = id;
		this.brand = brand;
		this.model = model;
		this.year = year;
		this.description = description;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getBrand() {
		return brand;
	}

	public String getModel() {
		return model;
	}

	public Integer getYear() {
		return year;
	}

	public String getDescription() {
		return description;
	}

	@Override
	public String toString() {
		return String.format("Car [id=%s, brand=%s, model=%s, year=%d, description=%s]",
				id, brand, model, year, description);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof Car)) return false;
		Car car = (Car) o;
		return Objects.equals(id, car.id) &&
				Objects.equals(brand, car.brand) &&
				Objects.equals(model, car.model) &&
				Objects.equals(year, car.year) &&
				Objects.equals(description, car.description);
	}

	@Override
	public int hashCode() {

		return Objects.hash(id, brand, model, year, description);
	}
}
