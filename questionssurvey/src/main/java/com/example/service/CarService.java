package com.example.service;

import com.example.model.Car;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CarService {
	private static List<Car> cars = new ArrayList<>();
	static {
		Car car1 = new Car("car1", "Audi", "80", 1991, "Old car. Very used.");
		Car car2 = new Car("car2", "BMW", "320", 2004, "Used car.");
		Car car3 = new Car("car3", "Volvo", "S80", 2007, "Swedish quality");

		cars.add(car1);
		cars.add(car2);
		cars.add(car3);
	}

	public Car retrieveCar(String carId) {
		for (Car car : cars) {
			if (car.getId().equals(carId)) {
				return car;
			}
		}
		return null;
	}

	public List<Car> retrieveCars() {
		if (cars != null)
			return cars;
		return null;
	}

	public Car addCar(Car car) {
		cars.add(car);
		return car;
	}
}
