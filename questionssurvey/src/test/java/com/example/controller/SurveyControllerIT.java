package com.example.controller;

import com.example.Application;
import com.example.jpa.UserCommandLineRunner;
import com.example.model.Question;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.skyscreamer.jsonassert.JSONAssert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;

import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class,
		webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class SurveyControllerIT {

	private static final Logger LOG = LoggerFactory.getLogger(UserCommandLineRunner.class);

	@LocalServerPort
	private int port;
	private TestRestTemplate restTemplate = new TestRestTemplate();
	HttpHeaders httpHeaders = new HttpHeaders();

	@Before
	public void before() {
		httpHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
	}

	@Test
	public void testJsonAssert() {
		String actual = "{id:1}";
		String expected = "{id:1}";
		JSONAssert.assertEquals(expected, actual, false);
	}

	@Test
	public void testRetrieveSurveyQuestion() {
		String retrieveSpecificQuestionUrl = "/surveys/Survey1/questions/Question1";
		String url = getUrl(retrieveSpecificQuestionUrl);

		HttpEntity entity = new HttpEntity<String>(null, httpHeaders);

		ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);

		LOG.info("Response: " + response.getBody());
		assertTrue(response.getBody().contains("\"id\":\"Question1\""));
		assertTrue(response.getBody().contains("\"description\":\"Largest Country in the World\""));

		String expected = "{id:Question1,description:Largest Country in the World,correctAnswer:Russia}";

		JSONAssert.assertEquals(expected, response.getBody(), false);
	}


	@Test
	public void retrieveAllQuestions() {
		String url = getUrl("/surveys");

		HttpEntity entity = new HttpEntity<String>(null, httpHeaders);

		ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);

		LOG.info("Response: {}", response.getBody());

		String expected = "[{id:Survey1}]";
		JSONAssert.assertEquals(expected, response.getBody(), false);
	}

	@Test
	public void addQuestion() {
		String retrieveAllQuestions = "/surveys/Survey1/questions";
		String url = getUrl(retrieveAllQuestions);

		Question question = new Question("DoesNotMatter",
				"Largest Country in the World", "Russia", Arrays.asList(
				"India", "Russia", "United States", "China"));

		HttpEntity entity = new HttpEntity<Question>(question, httpHeaders);
		ResponseEntity<String> response =
				restTemplate.exchange(url, HttpMethod.POST, entity, String.class);

		String actual = response.getHeaders().get(HttpHeaders.LOCATION).get(0);
		LOG.info("Actual: {}", actual);

		assertTrue(actual.contains("/surveys/Survey1/questions"));
	}

	private String getUrl(String retrieveSpecificQuestionUrl) {
		return "http://localhost:" + port + retrieveSpecificQuestionUrl;
	}


}
